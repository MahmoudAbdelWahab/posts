<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder;

class Post extends Model
{
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeWithLikes(Builder $query){
        $query->leftJoinSub(
            'select post_id,sum(liked) likes, sum(!liked) dislikes from likes group by post_id',
            'likes',
            'likes.post_id',
            'posts.id'
        );
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function like($user = null, $like = true)
    {
        $this->likes()->updateOrCreate(
            [
                'user_id' => $user ? $user->id:auth()->id(),
            ],
            [
                'liked' => $like
            ]
        );
    }

    public function dislike($user = null)
    {
        return $this->like($user,false);
    }

    public function isLikedBy(User $user){
        return (bool) $user->likes->where('post_id',$this->id)->where('liked',true)->count();
    }

    public function disLikedBy(User $user){
        return (bool) $user->likes->where('post_id',$this->id)->where('liked',false)->count();
    }
}
