<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class message extends Model
{
    protected $guarded;

    protected $fillable = [
        'user_id', 'body'
    ];

    // user function for relation with message Model
    public function user(){
        return $this->belongsTo('App\User');
    }
}
