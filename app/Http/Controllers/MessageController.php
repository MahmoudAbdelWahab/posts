<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\message;
use App\Events\MessageDelivered;

class MessageController extends Controller
{
    // construct function
    public function _construct(){
        $this-middleware(['auth']);
    }

    // index function return all messages
    public function index(){
        $messages = message::all();
        return view('messages.index',compact('messages'));
    }

    // store message to save message and send to other users.
    public function store(Request $request){
       $message = auth()->user()->messages()->create($request->all());
       broadcast(new MessageDelivered($message->load('user')))->toOthers();

    }
}
