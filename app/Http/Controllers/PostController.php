<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }


    public function index()
    {
        $posts = Post::all();

        return view('home')->with(compact('posts'));
    }

    public function create()
    {
        return view('post.index');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'unique:posts', 'max:255'],
            'description' => ['required'],
        ]);

        $post = new Post();
        $post->title = $request->get('title');
        $post->description = $request->get('description');
        $post->save();

        return redirect('home');

    }

    public function destroy($id)
    {

        $post = Post::find($id);

        $post->delete();

        return redirect('home');

    }

    public function show($id)
    {
        $post = Post::find($id);

        return view('post.show', compact('post'));
    }
}
