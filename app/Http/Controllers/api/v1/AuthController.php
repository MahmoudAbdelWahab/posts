<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use App\User;

class AuthController extends Controller
{
    // api register function
    public function Register(Request $request){
        $register = $request->validate([
            'name' => 'required',
            'email' => 'required|String',
            'phone' => 'required',
            'password' => 'required'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->save();

        $http = new Client;
        $response = $http->post('http://pos.local/oauth/token', [
          'form_params' => [
            'grant_type' => 'password',
            'client_id' => '2',
            'client_secret' => 'OMoCQhze8Aj7W2oJ4jo7YLbKP2zPYxZaDUKSL0lM',
            'username' => $request->email,
            'password' => $request->password,
            'scope' => '',
            ],
        ]);

        return json_decode((string) $response->getBody(), true);
    }

    // api login function
    public function login(Request $request){
        $login = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        if( !Auth::attempt($login) ){
            return response(['message'=>'Ivalid Login Credentials']);
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;
        
        return response(['user'=>Auth::user(),'access-token'=>$accessToken]);
   }
}
