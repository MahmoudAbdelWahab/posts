<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Messsage Chat Route
//Route::resource('messages','MessageController');


// Public Route
Route::get('/', function () {
    return view('welcome');
});

//Auth Routes(login - register)
Auth::routes();

// Home Route
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'post'], function () {
    Route::get('', 'PostController@index');
    Route::get('create', 'PostController@create')->name('post.create');
    Route::post('store', 'PostController@store')->name('post.store');
    Route::get('destroy/{id}', 'PostController@destroy')->name('post.destroy');
    Route::get('show/{id}', 'PostController@show')->name('post.show');

});
Route::group(['prefix' => 'comment'], function () {
    Route::post('/store', 'CommentController@store')->name('comment.add');
});

