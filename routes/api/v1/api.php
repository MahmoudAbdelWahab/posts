<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Users Api Routes
Route::prefix('user')->group(function(){
    Route::post('login','api\v1\AuthController@Login');
    Route::post('register','api\v1\AuthController@Register');
    Route::middleware('auth:api')->get('/all','api\v1\UserController@index');
});