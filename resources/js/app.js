
require('./bootstrap');


import Echo from "laravel-echo"

// required socket.io-client
window.io = require('socket.io-client');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});


// online event broadcast to show online auth here, joined and left users.
let onlineUsersLength =0;

window.Echo.join('online')
    .here((users) => {
        onlineUsersLength = users.length;

        if(users.length > 1){
            $('#no-online-users').css('display','none');
        }

        let userId = $('meta[name=user_id]').attr('content'); 
        users.forEach(function(user){
           if(user.id == userId){
               return;
           }

        $('#online-users').append(`<li id="user-${user.id}" class="list-group-item"><i class="fas fa-circle text-success"></i>${user.name}</li>`)
       })
    })
    .joining((user) => {
        onlineUsersLength++;
        $('#no-online-users').css('display','none');
        $('#online-users').append(`<li id="user-${user.id}" class="list-group-item"><i class="fas fa-circle text-success"></i>${user.name}</li>`)
    })
    .leaving((user) => {
        onlineUsersLength--;
        if(onlineUsersLength == 1){
            $('#no-online-users').css('display','block');
        }
        $('#user-'+user.id).remove();
    });

// chat-text is text in chat rom to send messages 
    $('#chat-text').keypress(function(e){

            if(e.which == 13){
            e.preventDefault();
            let body =$(this).val();
            let url = $(this).data('url');
            let userName = $('meta[name=user_name]').attr('content');

            $(this).val('');
            
            $('#chat').append(`
                <div class="p-2 bd-highlight text-black float-right bg-primary" style="margin-top:4px;width:40;">
                    <p>${userName}</p>
                    <p>${body}</p>
                </div>
                <div class="clearfix"></div>`
            );

            let data ={
                '_token' :$('meta[name=csrf-token]').attr('content'),
                body
            };
            
            $.ajax({
                url: url,
                method: 'POST',
                data: data,
            });
        }

    });

    // chat-group channel for append message to other listeners event
    window.Echo.channel('chat-group')
    .listen('MessageDelivered', (e) => {
        $('#chat').append(`
             <div class="p-2 bd-highlight text-black float-left bg-warning" style="margin-top:4px;width:40;">
                <p>$(user.name)</p>
                <p>$(e.message.body)</p>
            </div>
            <div class="clearfix"></div>`
        );
    });


    