@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            <div class="col-md-8 col-md-offset-2">

                <h1>Create post</h1>


                <form action="{{route('post.store')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label for="title">Title <span class="require">*</span></label>
                        <input type="text" class="form-control" name="title" required/>
                    </div>

                    <div class="form-group">
                        <label for="description">Description<span class="require">*</span></label>
                        <textarea rows="5" class="form-control" name="description" required ></textarea>
                    </div>

                    <div class="form-group">
                        <p><span class="require">*</span> - required fields</p>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" />
                    </div>

                </form>
            </div>

        </div>
    </div>

@endsection
