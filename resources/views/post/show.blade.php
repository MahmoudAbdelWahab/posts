@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <p style="font-weight: bold">{{ $post->title }}</p>
                        <p>
                            {{ $post->description }}
                        </p>
                        <div class="row">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                            <div class="col-md-1 ">
                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                <span>{{$post->likes? 1: 0}}</span>
                            </div>


                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                            <div class="col-md-1">
                                <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                <a>{{$post->dislikes ?: 0}}</a>
                            </div>

                        </div>
                        <hr/>
                        <h4>Display Comments</h4>
                        @foreach($post->comments as $comment)
                            <div class="display-comment">
                                <strong>{{ $comment->user->name }}</strong>
                                <p>{{ $comment->body }}</p>
                            </div>
                        @endforeach
                        <hr/>
                        <h4>Add comment</h4>
                        <form method="post" action="{{ route('comment.add') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                            <div class="form-group">
                                <input type="text" name="comment_body" class="form-control" required/>
                                <input type="hidden" name="post_id" id="post_id" value="{{ $post->id }}"/>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-warning add-comment" value="Add Comment"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
