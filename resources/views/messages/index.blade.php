@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-mid-3">
                <h3>Online Users</h3>

                <hr>

                <h5 id="no-online-users">no online users</h5>
                <ul class="list-group" id="online-users">
                </ul>
            </div> <!-- end of col-mid-3 div -->

            <hr>

            <div class="d-flex flex-column mb-2" style="height:80vh;">
                <div class="p-2" style="overflow-y:scroll;height:460px;background:white;width:960px;"  id="chat">

                    @foreach($messages as $message)
                        <div class="p-2 bd-highlight text-black {{ auth()->user()->id == $message->user_id ? 'float-right bg-primary' : ' float-left bg-warning'}}" style="margin-top:4px;width:40;">
                            <p>{{$message->user->name}}</p>
                            <p>{{$message->body}}</p>
                        </div>
                        <div class="clearfix"></div>
                    @endforeach

                </div><!-- end of p-2 div -->

                <form action="" class="d-flex" style="margin-top: 25px;">
                    <input id="chat-text" type="text" name="" data-url="{{route('messages.store')}}" style="margin-right:10px;width:888px;" class="form-control">
                    <button class="btn btn-primary">Send</button>
                </form>
            </div> <!-- end of flex-colum div -->
        </div> <!-- end of Row div -->
     </div> <!-- end of container div -->

@endsection
