@extends('layouts.app')

@section('content')
    <a style="margin-bottom: 10px;" href="{{ route('post.create') }}" class="btn btn-primary">Create Post</a>
    <table class="table table-sm table-dark">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Created_at</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($posts as $post )
            <tr>
                <th scope="row">{{$post->id}}</th>
                <td>{{$post->title}}</td>
                <td>{{$post->description}}</td>
                <td>{{$post->created_at}}</td>
                <td>
                    <a href="{{ route('post.show', $post->id) }}" class="btn btn-primary">Show</a>
                    <br>
                    <a href="{{ route('post.destroy', $post->id) }}" class="btn btn-warning">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
